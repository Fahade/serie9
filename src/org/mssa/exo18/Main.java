package org.mssa.exo18;

import java.lang.reflect.InvocationTargetException;

public class Main {

	public static void main(String[] args) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		// TODO Auto-generated method stub

		Person p1 = new Person();
		Person p2 = new Person();
		Employee e1 = new Employee();
		p1.setAge(20);
		p1.setLastName("Doe");
		p1.setFirstName("John");
		p2.setAge(30);
		p2.setLastName("Smith");
		p2.setFirstName("James");

		AnalyzeBean ab = new AnalyzeBean();
		String nomClass = ab.getClassName(p1);
		System.out.println("Nom de la classe de l'object : " + nomClass);
		System.out.println("Instance de la classe : " + ab.getInstance(nomClass));	
		System.out.println("Proprietes de la classe Person : " + ab.getProperties(p1));
		System.out.println("Proprietes de la classe Employee : " + ab.getProperties(e1));
		System.out.println("Valeur de la propriete lastName de la classe Person : " + ab.get(p1, "lastName"));
		System.out.println("Valeur de la propriete age de la classe Person : " + ab.get(p1, "age"));

		System.out.println("Personne 1 : " +p1);
		System.out.println("Personne 2 : " +p2);

		ab.set(p1, "lastName",p2.getLastName());
		//ab.set(p1, "firstName",p2.getFirstName());
		//ab.set(p1, "age",p2.getAge());

		System.out.println("Personne 1 : " +p1);
		

	}

}
