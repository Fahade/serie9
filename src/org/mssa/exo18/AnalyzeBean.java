package org.mssa.exo18;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class AnalyzeBean {

	public String getClassName(Object o) {

		return (String) o.getClass().getName();
	}

	@SuppressWarnings("deprecation")
	public Object getInstance(String className) {
		Object o = new Object();
		try {
			o = Class.forName(className).newInstance();
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return o;

	}

	public List<String> getProperties(Object o) {

		List<String> properties = new ArrayList<String>();
		Method[] methods = o.getClass().getMethods();
		for (Method m : methods) {

			properties.add(m.getName());
		}

		properties = properties.stream().filter(s -> s.startsWith("set") || s.startsWith("get"))
				.map(s -> s.substring(3, s.length())).distinct().collect(Collectors.toList());

		return properties;
	}

	public Object get(Object bean, String property) {

		Object o = null;
		property = property.substring(0, 1).toUpperCase().concat(property.substring(1)); // mettre la 1ere lettre en maj
		try {
			o = bean.getClass().getMethod("get" + property).invoke(bean, new Object[] {});
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return o;

	}

	public void set(Object bean, String property, Object value) {
		Method m = null;
		property = property.substring(0, 1).toUpperCase().concat(property.substring(1)); // mettre la 1ere lettre en maj

		try {
			m = bean.getClass().getMethod("set" + property, value.getClass());
			m.invoke(bean, value);
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
